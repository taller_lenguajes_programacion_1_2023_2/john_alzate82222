import tkinter as tk
from tkinter import messagebox, ttk
from Pelicula import Pelicula
class interfaz:
    def __init__ (self):
        self.ventanaPrincipal = tk.Tk()
        self.ventanaPrincipal.title("BASE DE DATOS")
        self.ventanaPrincipal.resizable(False,False)
        self.FrameDatosPelicula = tk.Frame(self.ventanaPrincipal)
        self.FrameDatosPelicula.grid(row=0, column=0, padx=5, pady=5)
        self.FrameBotonSuperior =tk.Frame(self.ventanaPrincipal)
        self.FrameBotonSuperior.grid(row=1, column=0, padx=5,pady=5)
        self.frameBaseDeDatos = tk.Frame(self.ventanaPrincipal)
        self.frameBaseDeDatos.grid(row=2,column=0)
        self.frameBotonLateral =tk.Frame(self.ventanaPrincipal)
        self.frameBotonLateral.grid(row=2,column=1,padx=5)
        
        
        self.pelicula = Pelicula()
        self.CamposDeTexto()
        self.botonesSuperiores()
        self.crearTablaInterfaz()
        self.vincularTabla()
        self.botonLateral()
        self.ventanaPrincipal.mainloop()
    
    def crearTablaInterfaz(self):
        self.tablaBaseDeDatos = ttk.Treeview(self.frameBaseDeDatos,show="headings")
        self.tablaBaseDeDatos.config(columns=("ID", "NOMBRE", "DURACION", "GENERO"))
        self.tablaBaseDeDatos.heading("ID", text="ID")
        self.tablaBaseDeDatos.heading("NOMBRE", text="NOMBRE")
        self.tablaBaseDeDatos.heading("DURACION", text="DURACION")
        self.tablaBaseDeDatos.heading("GENERO", text="GENERO")
        self.tablaBaseDeDatos.grid(row=0,column=0)
        
    
    def CamposDeTexto(self):
        self.varNombre = tk.StringVar()
        self.textoNombre =tk.Label(self.FrameDatosPelicula, text="Nombre: ")
        self.textoNombre.grid(row=0,column=0)
        self.cuadroNombre = tk.Entry(self.FrameDatosPelicula, textvariable=self.varNombre)
        self.cuadroNombre.grid(row=0,column=1)
        
        self.varDuracion = tk.StringVar()
        self.textoDuracion =tk.Label(self.FrameDatosPelicula, text="Duracion: ")
        self.textoDuracion.grid(row=1,column=0)
        self.cuadroDuracion = tk.Entry(self.FrameDatosPelicula,textvariable=self.varDuracion)
        self.cuadroDuracion.grid(row=1,column=1)
        
        self.varGenero = tk.StringVar()
        self.textoGenero =tk.Label(self.FrameDatosPelicula, text="Genero: ")
        self.textoGenero.grid(row=2,column=0)
        self.cuadroGenero = tk.Entry(self.FrameDatosPelicula,textvariable=self.varGenero)
        self.cuadroGenero.grid(row=2,column=1)
    
    def botonesSuperiores(self):
        self.botonNuevo= tk.Button(self.FrameBotonSuperior, text="NUEVO", bg="yellow", command=self.funcionNuevo)
        self.botonNuevo.grid(row=0,column=0, padx=5)
        
        self.botonGuardar= tk.Button(self.FrameBotonSuperior, text="GUARDAR", bg="yellow", command=self.FuncionGuardar)
        self.botonGuardar.grid(row=0,column=1,padx=5)
        
        self.botonCancelar= tk.Button(self.FrameBotonSuperior, text="CANCELAR", bg="yellow", command=self.funcionCancelar)
        self.botonCancelar.grid(row=0,column=2, padx=5)
    
    def funcionCancelar(self):
        self.varNombre.set('')
        self.varDuracion.set('')
        self.varGenero.set('')       
        self.cuadroNombre.config(state='disabled')
        self.cuadroDuracion.config(state='disabled')
        self.cuadroGenero.config(state='disabled')     
        self.botonGuardar.config(state='disabled')
        self.botonCancelar.config(state='disabled')
        
    def funcionNuevo(self):
        self.varNombre.set('')
        self.varDuracion.set('')
        self.varGenero.set('')       
        self.cuadroNombre.config(state='normal')
        self.cuadroDuracion.config(state='normal')
        self.cuadroGenero.config(state='normal')     
        self.botonGuardar.config(state='normal')
        self.botonCancelar.config(state='normal')
        
    def FuncionGuardar (self):
    
        self.pelicula.nombre = self.varNombre.get()
        self.pelicula.duracion = self.varDuracion.get()
        self.pelicula.genero = self.varGenero.get()
        self.pelicula.agregarDatos()
        self.vincularTabla()
        
    def vincularTabla(self):
        self.pelicula.verTabla()
        self.tablaBaseDeDatos.delete(*self.tablaBaseDeDatos.get_children())
        for fila in self.pelicula.datosEnLaTabla:
            self.tablaBaseDeDatos.insert("","end",values=fila)
    
    def botonLateral(self):
        self.varId =tk.IntVar()
        self.textoId = tk.Label(self.frameBotonLateral, text="ID: ")
        self.textoId.grid(row=0,column=0,padx=5,pady=5)
        self.cuadroId = tk.Entry(self.frameBotonLateral, textvariable=self.varId)
        self.cuadroId.grid(row=1,column=0,padx=5,pady=5)
        
        self.botonEditar= tk.Button(self.frameBotonLateral, text="EDITAR", bg="yellow", command=self.funcionEditar)
        self.botonEditar.grid(row=2,column=0, padx=5,pady=5)
        
        self.botoneliminar= tk.Button(self.frameBotonLateral, text="ELIMINAR", bg="yellow", command=self.funcionEliminar)
        self.botoneliminar.grid(row=3,column=0,padx=5,pady=5)
        
    def funcionEditar(self):
        if self.varNombre.get():
            self.pelicula.actualizarDatos("peliculas","NOMBRE",self.varNombre.get(),self.varId.get())
        if self.varDuracion.get():
            self.pelicula.actualizarDatos("peliculas","DURACION",self.varDuracion.get(),self.varId.get())    
        if self.varGenero.get():    
            self.pelicula.actualizarDatos("peliculas","GENERO",self.varGenero.get(),self.varId.get())
        self.vincularTabla()
    
    def funcionEliminar(self):
        self.pelicula.eliminarDatos("peliculas",self.varId.get())
        self.vincularTabla()
        
aplicacion = interfaz() 
        