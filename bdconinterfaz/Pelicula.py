from conexion import Conexion

class Pelicula(Conexion):
    def __init__(self, nombre="", duracion="", genero=""):
        Conexion.__init__(self)
        self.nombre= nombre
        self.duracion= duracion
        self.genero = genero
        self.crearTabla("peliculas", "Pelicula")
    
    def agregarDatos(self):
        
        datos = [
            (f"{self.nombre}",f"{self.duracion}",f"{self.genero}")
        ]
        if self.nombre !="" and self.duracion!="" and self.genero !="":
            self.insertarDato("peliculas",3,datos)
        
    def verTabla(self):
        self.datosEnLaTabla = self.seleccionarTabla("peliculas")
        