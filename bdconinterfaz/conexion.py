from tkinter import messagebox
import sqlite3
import json

class Conexion:
    def __init__(self):
        self.BaseDeDatos = sqlite3.connect("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/bdconinterfaz/BaseDeDatos.sqlite")
        self.Apuntador = self.BaseDeDatos.cursor()
        with open ("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/bdconinterfaz/Queries.json","r") as queries:
            self.query = json.load(queries)
    
    def crearTabla(self, Nombre,columnas):
        queryCrearTabla = self.query ["CrearTabla"].format(Nombre, self.query[columnas])
        self.Apuntador.execute(queryCrearTabla)
        self.BaseDeDatos.commit()
        
    def insertarDato(self, tabla, columnas, datos):
        if tabla != "" and columnas != "" and datos != "":
            queryInsertarDatos = self.query["InsertarDatos"].format(tabla,', '.join(['?'] * columnas))
            self.Apuntador.executemany(queryInsertarDatos,datos)
            self.BaseDeDatos.commit()
            messagebox.showinfo("EXITO","GUARDADO CON EXITO")
        else:
            messagebox.showinfo("ERROR","FALTAN CAMPOS POR LLENAR")
        
    def seleccionarTabla(self, tabla):
        querySeleccionarTabla = self.query["SeleccionarTodo"].format(tabla)
        self.Apuntador.execute(querySeleccionarTabla)
        contenido = self.Apuntador.fetchall()
        return contenido

    def actualizarDatos(self,tabla,columna,nuevoValor,id):
        queryActualizarDatos = self.query["ActualizarDatos"].format(tabla,columna,nuevoValor,'ID',id)
        self.Apuntador.execute(queryActualizarDatos)
        self.BaseDeDatos.commit()
    
    def eliminarDatos(self, tabla,id):   
        queryEliminarDatos = self.query["EliminarDatos"].format(tabla,'ID',id)
        self.Apuntador.execute(queryEliminarDatos)
        self.BaseDeDatos.commit()
        messagebox.showinfo("EXITO", "ELIMINADO CORRECTAMENTE")

    
            