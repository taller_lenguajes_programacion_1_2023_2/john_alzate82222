#clase 4, guardar excel en una variable

import pandas as pd

convertir=pd.read_excel("Tabla2.xlsx") # Guarda el DataFrame en una variable
# convertir.loc[Fila,"Fecha"]="23/08/2023" Agregar nueva columna al excel
# convertir.loc[len(convertir)]= ["Juanjo",19,"Informatica",31051502,"23/08/203"] Agregar nueva fila al excel
# nombre=convertir.loc[Fila,"Nombre"] Obtener el dato en esa casilla
# print(convertir.columns) Imprime el nombre de las columnas
# print(convertir.dtypes) Tipos de datos en columna (object para string, int64 para numeros)
# print(convertir.iloc[Fila]) Imprime una fila determinada
# print(convertir["Nombre"]) Imprime un columna determinada
# print(convertir["Nombre"].iloc[Fila]) Imprime el nombre en la fila especificada

#agregar al excel
convertir.loc[len(convertir)]= ["Jhonkii",25,"Informatica",31051502]
convertir.loc[len(convertir)]= ["manuela",25,"contadora",312214314]
print(convertir)
#guardar excel, poner hubicacion
convertir.to_excel("C:/Users/JOHN KIDER\OneDrive\Escritorio/taller de lenguaje/john_alzate82222/Tabla3.xlsx",index=False)