#clase 6 taller
#1 hola mundo
#print ("Hola Mundo")

#2variables y asignacion
#nombre ="Johnki"
#numero = 5
#numerof= 5.5
#bool = True

#3tipos de variables basicos
#print (type(nombre))
#print (type(numero))
#print(type(numerof))
#print(type(bool))

#tipos de datos basicos
#print (4*4)
#print (4+4)
#print (4+3)
#print (4/4)

#conversion entre tipos de datos
#edad = str(nombre)
#print (edad)

#solicitar entrada de payton
#entrada = input("ingrese su edad")
#print (entrada)

#condicional if
#if entrada <18:
#    print ("eres menor de edad")

#condicional if-else
#if entrada<18:
#    print("eres menor de edad")
#else:
#    print("ya creciste")

#condicional if elif else

#if entrada <18:
#    print("menor de edad")
#elif entrada == 18:
#    print("saque la cedula")
#else:
#    print ("eres mayor de edad")

#bucle for

#lista = [1,2,3,4,5,6]
#for i in range(6):
#    print (lista)

#ciclo while

#i =0
#while i<10:
#    print(i)
#    i=i+1

#uso de break y continue = continue es para ignorar las siguientes lineas de codigo

#i =0
#while i<10:
#    print(i)
#    i=i+1
#    break
#while i<10:
#    print(i)
#    continue
#    i=i+1

#listas y sus operaciones basicas

#lista= [1,2,3,4,5,6]
#agregar lista.append(7)
# lista.insert(3,7)
#print (lista)
# eliminar lista.remove(2)
#print (lista)
# cambiar o asignar valor a posicion lista[2]= 44

#tuplas es inmutable, para parametrizar valores

#tupla = (1,2,3,4,5,6,)
#print (tupla)
#tupla.count(devuelve el valor de la posicion)
#tupla. index (devuelve la posicion)

#conjunto de datos y sus operaciones
#son mas eficientes que las listas
#conjuntos = {1,2,3,4,5,6}
#conjuntos.add(7)
#conjuntos.remove(1)
#print (conjuntos)
#cambiar todo el conjunto conjuntos= set ({2})
#print(conjuntos)

#diccionarios
#definicion diccionarios = {
#   "nombre": "john",
#    "edad" : 25 ,
#    "fecha" : "23/12/2023"
#} 
#añadir al diccionario diccionarios["apellido"]= "marin"
#eliminar del diccionario del diccionarios ["fecha"]
#print(diccionarios)

#list funciones basicas
#lista= [1,2,3,4,5,6]
#agregar lista.append(7)
# lista.insert(3,7)
#print (lista)
# eliminar lista.remove(2)
#print (lista)
# cambiar o asignar valor a posicion lista[2]= 44

#leer un archivo de texto
#crear se copia la direccion donde se va a crear el archivo
#with open ("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto 18.txt","w")as txt:
#    txt.write ("LA VIDA ES MARAVILLOSA")
#agregar(ecribir) al archivo
#with open ("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto 18.txt","a")as txt:
#    txt.write("\t JOHN KIDER ALZATE GARCIA")
#leer el archivo
#with open ("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto 18.txt","r")as txt:
#    print(txt.read())

#escribir un archivo de texto

#with open ("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto 18.txt","a")as txt:
#    txt.write("\nJOHN KIDER ALZATE GARCIA")

#modos de apertura w crear, a agregar, r leer
#crear se copia la direccion donde se va a crear el archivo
#with open ("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto 18.txt","w")as txt:
#    txt.write ("LA VIDA ES MARAVILLOSA")
#agregar(ecribir) al archivo
#with open ("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto 18.txt","a")as txt:
#    txt.write("\t JOHN KIDER ALZATE GARCIA")
#leer el archivo
#with open ("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto 18.txt","r")as txt:
#    print(txt.read())

#archivos Json trabajar con ellos
#import pandas as pd

#punto21json= pd.DataFrame ({
#    "nombre": ["cris\n","johnki\n","juano\n"],
#    "edades":[20,25,19]
#})
#punto21json.to_json("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto21json.json")

#leer un archivo CSV
#el "; es como estan separada las palabras en las filas y columnas"
#import pandas as pd
#archivocsv=pd.read_csv("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto22csv.txt",sep=";")
#print(archivocsv)

#filtrar datos en un dataframe entre [""]se pone la columna que se quiera filtrar

#filtrar= archivocsv["Fecha"]
#print (filtrar)

#operaciones basicas sum() mean() max() min()
#import numpy as np

#operacion=[1,2,3,4,5]
#suma print (np.sum(operacion))
# promedio print (np.mean(operacion))
#numero maximo print (np.max(operacion))
#numero minimo print (np.min(operacion))

#uso de iloc y loc
import pandas as pd
punto25=pd.DataFrame ({
    "nombre":["jk","cr","jn"],
    "edad": [15,20,30]
})
#iloc saca todos los datos de esa posicion
#loc hay que especificarle de donde sacar el dato y la posicion
#print (punto25.iloc[2])
#print(punto25.loc[1]["nombre"])
#print(punto25.loc[1]["edad"])

#agrupar datos con groupby

#punto25.groupby("edad")
#for edad in punto25.groupby("edad"):
#    print (edad)

#unir DataFrame con merge y conccat

#punto27=pd.DataFrame({
#    "nombre":["jk","cr","jn"],
#    "doc":["doc1","doc2","doc3"],
#    "nota":[15,20,30]
#})
#unir los dos dataFrame
#print("concat")
#print (pd.concat([punto25,punto27]))
#mira los datos iguales y le agrega la nueva columna
#print("merge")
#print(pd.merge(punto25,punto27,on="nombre"))

#manipular series temporales

#valores=(1,2,3,4,5)
#print(pd.Series(valores))

#exportar DataFrame a csv
#con la direccion de donde se vaya a crear y nombre "punto29..."
#punto27.to_csv("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto29.csv")

#convertir DataFrame a json

#punto27.to_json("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto30.json")

#lee una tabla html y guardalo en DataFrame
import pandas as pd
guardadora=pd.read_html("https://help.netflix.com/es/node/24926")[0]
print(guardadora)

#guardar tabla html en excel

guardadora.to_excel("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto32.xlsx")

#exportar los primeros 10 registros del dataFrame anterior a un archivo csv

htmlacsv=pd.read_html("https://www.colombia.com/cambio-moneda/monedas-del-mundo/")[0]
htmlacsv.head(10).to_csv("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto33.csv")

#lee 3 tablas html y guardalas en 3 hojas diferentes de un archivo excel

percapita= pd.read_html("https://www.worldometers.info/gdp/gdp-per-capita/")[0]
with pd.ExcelWriter("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto34.xlsx")as writer:
    guardadora.to_excel(writer,sheet_name= "netflix")
    htmlacsv.to_excel(writer,sheet_name="monedas")
    percapita.to_excel(writer,sheet_name="percapita")

#lee un archivo excel, filtra los registros donde la columna edad sea mayor a 30 y guardalo

filtro35=punto25.loc[punto25["edad"]>19]
print(filtro35)
filtro35.to_csv("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto35.csv")

#lee un archivo csv y filtra la columna "cualquiera" y guardalo en un excel

filtro36=guardadora["Planes de Netflix"]
filtro36.to_excel("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto36.xlsx")

#lee la primer y tercer hoja del archivo excel y combinalas en un dataframe

leedora=pd.ExcelFile("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto34.xlsx")
net=pd.read_excel(leedora,"netflix")
per=pd.read_excel(leedora,"percapita")
combinadora=pd.concat([net,per])
combinadora.to_excel("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/punto37.xlsx")
