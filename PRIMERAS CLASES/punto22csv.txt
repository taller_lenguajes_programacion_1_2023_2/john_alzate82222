N° sesión;Fecha;Contenido por desarrollar;Descripción del trabajo presencial; Descripción trabajo independiente
1;9/08/2023;Introducción a archivos planos;Presentación de la unidad y ejemplos básicos;Lectura recomendada sobre archivos en Python
2;10/08/2023;Leer archivos planos;Práctica de lectura de archivos;Ejercicios de lectura de archivos
3;16/08/2023;Escribir archivos planos;Práctica de escritura en archivos;Ejercicios de escritura de archivos
4;17/08/2023;Manipulación de archivos CSV;