from django.shortcuts import render,redirect
from .models import clientes
# Create your views here.

def pagInicio(request):
    return render(request, 'index.html')

def pagLogin(request):
    mensajeError = ""
    if request.method == 'POST':
        usuario = request.POST['usuario']
        clave = request.POST['Password']
        
        
        try:
            usuarioEncontrado = clientes.objects.get(USUARIO = usuario)
            if usuarioEncontrado.PASSWORD == clave:
                return redirect('/Categorias/')
            else:
                mensajeError = "Contraseña Incorrecta."
                
        except:
            mensajeError = "Usuario No Encontrado."
        
    return render(request, 'login.html', {
        "error": mensajeError
    })

def pagRegistro(request):
    mensajeFail=""
    if request.method == 'POST':
        nombre= request.POST['nombre']
        primerApellido= request.POST['priapellido']
        secondApellido= request.POST['seapellido']
        email= request.POST['email']
        usuario= request.POST['usuario']
        clave= request.POST['clave']
        
        try:
            usuarioConocido= clientes.objects.get(USUARIO=usuario)
            mensajeFail="El Usuario Ya Existe"
        except:
            nuevoCliente = clientes.objects.create(NOMBRE=nombre, P_APELLIDO=primerApellido,S_APELLIDO=secondApellido,
                                               EMAIL=email,USUARIO=usuario,PASSWORD=clave)
            return redirect('Login/')
        
    return render(request,'registro.html', {
        "error": mensajeFail
    })

def ayuda(request):
    return render(request,'ayuda.html')