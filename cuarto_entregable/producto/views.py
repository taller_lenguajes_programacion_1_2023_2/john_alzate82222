from django.shortcuts import render
# Create your views here.

def Categorias(request):
    return render(request,'categorias.html')

def infoCategorias(request, categoria):
    return render(request, 'infoCategorias.html', {
        "categoria":categoria
    })
def compras(request,compra):
    return render(request,'compra.html',{
        "compra":compra
    })
