import pandas as pd
#5 escribe un programa que determine si un numero n es impar o par

jkag_n= int(input("ingrese un numero "))
if jkag_n % 2 == 0 :
    print("es par")
else:
    print ("es inpar")

#15 escribe un programa que determine si una cadena s es un palindromo o no
def es_palindromo(jkag_s):
  jkag_s = jkag_s.lower()
  for i in range(len(jkag_s) // 2):
    if jkag_s[i] != jkag_s[len(jkag_s) - 1 - i]:
      return False
  return True
jkag_a = input("Ingrese una cadena: ")
if es_palindromo(jkag_a):
  print("La cadena {} es un palíndromo".format(jkag_a))
else:
  print("La cadena {} no es un palíndromo".format(jkag_a))


#25 dada una lista escribe un programa que devuelva la suma de todos los elementos
jkag_lista= [1,2,3,4,5,6]
jkag_suma=0

for elemento in jkag_lista:
    jkag_suma+=elemento

print(jkag_suma)

#35 escribe un programa que combine dos diccionarios en uno

jkag_diccionario1 ={
    "nombre": "john",
    "edad" : 25 ,
    "fecha" : "23/12/2023"
}
jkag_diccionario2={
    "nombre2": "camilo",
    "edad2" : 30 ,
    "fecha2" : "25/12/2023"
}
jkag_diccionario1 .update(jkag_diccionario2)
print(jkag_diccionario1)

#45 escribe un programa que añada una nueva fila a un archivo csv

with open("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/1erEntregable/jkag_archivo.csv","w")as csv:
    csv.write("ARCHIVO PARA PUNTO 45")
with open("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/1erEntregable/jkag_archivo.csv","a")as csv:
    csv.write("\nAGREGANDO NUEVA FILA")

#55 SIMULE UN ARCHIVO CSV, ESCRIBE UN PROGRAMA QUE AGRUPE LOS DATOS POR UNA COLUMNA ESPECIFICA Y CALCULE LA SUMA DE CADA COLUMNA PARA CADA GRUPO
import pandas as pd
jkag_datos = [
    ["a", 1, 2, 3],
    ["b", 4, 5, 6],
    ["c", 7, 8, 9],
    ["d", 10, 11, 12],
    ["e", 13, 14, 15],
]
jkag_grouped_data={}
for row in jkag_datos:
  key = row[0]
  if key not in jkag_grouped_data:
    jkag_grouped_data[key] = []
  jkag_grouped_data[key].append(row)

for key, rows in jkag_grouped_data.items():
  jkag_sums = {}
  for column in range(1, len(jkag_datos[0])):
    jkag_sums[column] = 0
  for row in rows:
    for column, value in enumerate(row):
      if column > 0:
        jkag_sums[column] += value
  print(f"Grupo {key}:")
  for column, sum in jkag_sums.items():
    print(f"Columna {column}: {sum}")

#65 simula un archivo excel y filtra solo aquellos empleados cuyo salario sea mayor a 3000

jkag_datos = {
    "Nombre": ["Juan", "Pedro", "María", "José", "Luis"],
    "Edad": [25, 30, 22, 35, 28],
    "Salario": [2500, 3500, 3000, 4000, 3200],
}
jkag_jk = pd.DataFrame(jkag_datos)

jkag_filtered_jk = jkag_jk[jkag_jk["Salario"] > 3000]
print(jkag_filtered_jk)

#75 escribe un programa que lea un excel y cambie el nombre de una columna especifica
df=pd.DataFrame(jkag_datos)
df.to_excel("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/1erEntregable/jkag_archivo77.xlsx")
jkag_file_path = "C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/1erEntregable/jkag_archivo77.xlsx"
jkag_column_index = df.columns.get_loc("Nombre")
jkag_new_column_name = "APODO"
df.columns[jkag_column_index] = jkag_new_column_name
df.to_excel(jkag_file_path, index=False)

#85 simula un archivo csv productos.csv escribe un programa que se vendieron mas de 1000 veces pero que tienen una calificacion promedio menor a 3

def simular_productos_csv():
  with open("productos.csv", "w", newline="") as archivo:
      writer = csv.writer(archivo)

      for i in range(1000):
        producto = {
          "id": i,
          "nombre": "Producto {}".format(i),
          "precio": i * 10,
          "calificaciones": [i % 5 for i in range(1000)]
        }
        writer.writerow([producto["id"], producto["nombre"], producto["precio"], producto["calificaciones"]])

  return writer
def imprimir_jkag_productos_malos(writer):
  jkag_productos = []
  for row in writer:
    if row[0] > 1000 and sum(row[3]) / len(row[3]) < 3:
      jkag_productos.append(row)
  print("Los siguientes productos se vendieron más de 1000 veces pero tienen una calificación promedio menor a 3:")
  for producto in jkag_productos:
    print("Id: {}, Nombre: {}, Precio: {}, Calificaciones: {}".format(producto[0], producto[1], producto[2], producto[3]))


if __name__ == "__main__":
  writer = simular_productos_csv()
  imprimir_jkag_productos_malos(writer)
writer = simular_productos_csv()
imprimir_jkag_productos_malos(writer)

#95 escribe un programa que lea archivos csv y utilice la funcion merge para combinarlo con otro csv basado en una columna especifica

jkag_punto95=pd.DataFrame({
    "nombre":["jk","cr","jn"],
    "doc":["doc1","doc2","doc3"],
    "nota":[15,20,30]
})
jkag_punto95_2=pd.DataFrame ({
    "nombre":["jk","cr","jn"],
    "edad": [15,20,30]
})
print(pd.merge(punto25,punto27,on="nombre"))