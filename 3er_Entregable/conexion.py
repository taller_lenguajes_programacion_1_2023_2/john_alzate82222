import sqlite3
import json
#CRUD
class Conexion :
    #DEFINICION DE CONSTRUCTOR DONODE SE DA UBICACION Y NOMBRE A LA BD
    def __init__(self):
        self.BaseDeDatos = sqlite3.connect("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/3er_Entregable/BaseDeDatos.sqlite")
        self.Apuntador = self.BaseDeDatos.cursor()
        with open ("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/3er_entregable/Query.json","r") as queries:
            self.query = json.load(queries)
    #CREACION DE BASE DE DATOS
    def crearTabla(self, Nombre,columnas):
        queryCrearTabla = self.query ["CrearTabla"].format(Nombre, self.query[columnas])
        self.Apuntador.execute(queryCrearTabla)
        self.BaseDeDatos.commit()
    #INSERTAR DATOS
    def insertarDato(self, tabla, columnas, datos):
        queryInsertarDatos = self.query["InsertarDatos"].format(tabla,', '.join(['?'] * columnas))
        self.Apuntador.executemany(queryInsertarDatos,datos)
        self.BaseDeDatos.commit()    
    #FUNCION PARA VER LA TABLA
    def seleccionarTabla(self, tabla):
        querySeleccionarTabla = self.query["SeleccionarTodo"].format(tabla)
        self.Apuntador.execute(querySeleccionarTabla)
        contenido = self.Apuntador.fetchall()
        return contenido
    #FUNCION UPDATE PARA ACTULIZAR LA BD
    def actualizarDatos(self,tabla,columna,nuevoValor,id):
        queryActualizarDatos = self.query["ActualizarDatos"].format(tabla,columna,nuevoValor,'ID',id)
        self.Apuntador.execute(queryActualizarDatos)
        self.BaseDeDatos.commit()
    #ELIMINAR DATOS DE LA BD
    def eliminarDatos(self, tabla,id):
        queryEliminarDatos = self.query["EliminarDatos"].format(tabla,'ID',id)
        self.Apuntador.execute(queryEliminarDatos)
        self.BaseDeDatos.commit()
    