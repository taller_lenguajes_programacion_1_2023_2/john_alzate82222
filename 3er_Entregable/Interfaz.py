import tkinter as tk
from tkinter import messagebox, ttk
import sv_ttk
from Farm import farm
from Crop import crop
class interfaz:
    def __init__ (self):
        """
        Constructor de la clase. Inicializa la interfaz de usuario y sus componentes.
        """
        self.Farm = farm()
        self.Crop = crop()
        self.ventanaPrincipal = tk.Tk()
        self.ventanaPrincipal.title("BASE DE DATOS FARM AND CROP")
        self.ventanaPrincipal.resizable(False,False)
        try:
            self.ventanaPrincipal.iconbitmap("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/3er_Entregable/icono.ico")
        except:
            print("no se pudo cargar el icono")
        sv_ttk.use_dark_theme()
        self.FrameDatosFarmCrop = tk.Frame(self.ventanaPrincipal)
        self.FrameDatosFarmCrop.grid(row=0, column=0, padx=5, pady=5)
        self.FrameBotonSuperior =tk.Frame(self.ventanaPrincipal)
        self.FrameBotonSuperior.grid(row=1, column=0, padx=5,pady=5)
        self.frameBaseDeDatos = tk.Frame(self.ventanaPrincipal)
        self.frameBaseDeDatos.grid(row=2,column=0)
        self.FrameBotonInferior =tk.Frame(self.ventanaPrincipal)
        self.FrameBotonInferior.grid(row=3,column=0,padx=5)
       
        self.CamposDeTexto()
        self.botonesSuperiores()
        self.crearTablaInterfaz()
        self.crearTablaInterfazCrop()
        self.vincular()
        self.vincularTabla()
        self.botonInferior()
        self.centrarVentana()
        self.ventanaPrincipal.mainloop()
    
    def centrarVentana(self):
        self.ventanaPrincipal.update_idletasks()
        ancho = self.ventanaPrincipal.winfo_width()
        alto = self.ventanaPrincipal.winfo_height()

        anchoPantalla = self.ventanaPrincipal.winfo_screenwidth()
        altoPantalla = self.ventanaPrincipal.winfo_screenheight()

        x = (anchoPantalla - ancho) // 2
        y = (altoPantalla - alto) // 2

        self.ventanaPrincipal.geometry(f"{ancho}x{alto}+{x}+{y}")
    #SE CREA LA VENTANA CON LOS DATOS DE LA BASE DE DATOS Y SE LLAMA A LA INTERFAZ EN UN FRAME
    def crearTablaInterfaz(self):
        self.tablaBaseDeDatos = ttk.Treeview(self.frameBaseDeDatos,show="headings")
        self.tablaBaseDeDatos.config(columns=("ID", "NOMBRE GRANJA", "TAMAÑO GRANJA"))
        self.tablaBaseDeDatos.heading("ID", text="ID")
        self.tablaBaseDeDatos.heading("NOMBRE GRANJA", text="NOMBRE GRANJA")
        self.tablaBaseDeDatos.heading("TAMAÑO GRANJA", text="TAMAÑO GRANJA")
        self.tablaBaseDeDatos.grid(row=0,column=0)
    
     #SE CREA LA VENTANA CON LOS DATOS DE LA BASE DE DATOS Y SE LLAMA A LA INTERFAZ EN UN FRAME
    def crearTablaInterfazCrop(self):
        self.baseDeDatosCrop = ttk.Treeview(self.frameBaseDeDatos,show="headings")
        self.baseDeDatosCrop.config(columns=("Id","Nombre Cultivo","Id cultivo"))
        self.baseDeDatosCrop.heading("Id", text="ID")
        self.baseDeDatosCrop.heading("Nombre Cultivo", text="NOMBRE CULTIVO")
        self.baseDeDatosCrop.heading("Id cultivo", text="ID CULTIVO")
        self.baseDeDatosCrop.grid(row=0,column=1)    
    
    #SE DEFINEN LOS CAMPOS DE TEXTO Y LOS LABEL PARA INDICAR AL USUARIO LO QUE DEBE LLENAR EN ESE CAMPO
    def CamposDeTexto(self):
        self.varNombre = tk.StringVar()
        self.textoNombreGranja =tk.Label(self.FrameDatosFarmCrop, text="NOMBRE GRANJA: ")
        self.textoNombreGranja.grid(row=0,column=0)
        self.cuadroNombreGranja = tk.Entry(self.FrameDatosFarmCrop, textvariable=self.varNombre)
        self.cuadroNombreGranja.grid(row=0,column=1)
        
        self.varTamaño = tk.IntVar()
        self.textoTamañoGranja =tk.Label(self.FrameDatosFarmCrop, text="TAMAÑO GRANJA 'MTS': ")
        self.textoTamañoGranja.grid(row=1,column=0)
        self.cuadroTamañoGranja = tk.Entry(self.FrameDatosFarmCrop,textvariable=self.varTamaño)
        self.cuadroTamañoGranja.grid(row=1,column=1)
        
        self.separacion= tk.Label(self.FrameDatosFarmCrop, text="")
        self.separacion.grid(row=2, column=1)
        
        self.varNombreCultivo = tk.StringVar()
        self.textoNombreCultivo =tk.Label(self.FrameDatosFarmCrop, text="NOMBRE CULTIVO: ")
        self.textoNombreCultivo.grid(row=3,column=0)
        self.cuadroNombreCultivo = tk.Entry(self.FrameDatosFarmCrop, textvariable=self.varNombreCultivo)
        self.cuadroNombreCultivo.grid(row=3,column=1)
        
        self.varCultivoId = tk.IntVar()
        self.IdCultivo =tk.Label(self.FrameDatosFarmCrop, text="ID CULTIVO: ")
        self.IdCultivo.grid(row=4,column=0)
        self.cuandroIdCultivo = tk.Entry(self.FrameDatosFarmCrop,textvariable=self.varCultivoId)
        self.cuandroIdCultivo.grid(row=4,column=1)
    
    #CREACION DE BOTONES, SE HIZO DIFERENTES BOTONES DE GUARDAR PARA LAS DIFERENTES FORMAS DE GUARDADO
    #GUARDAR FARM SIRVE PARA GUARDAR LOS DATOS SOLO DE LA GRANJA COMO SU NOMBRE Y TAMAÑO
    #GUARDAR CROP SIRVE PARA GUARDAR LOS DATOS SOLO DEL CULTIVO COMO SU NOMBRE Y ID
    #GUARDAR todo SIRVE PARA GUARDAR AMBOS CAMPOS PERO AMBOS DEBEN DE ESTAR COMPLETAMENTE LLENOS.
    #DE LO CONTRARIO SOLO SE INCORPORARAN EN LA BASE DE DATOS , LOS QUE TENGAN AMBOS CAMPOS DILIGENCIADOS
    def botonesSuperiores(self):
        self.textoFARM =tk.Label(self.FrameBotonSuperior, text="FARM - GRANJA", font=("Arial",16,"bold"))
        self.textoFARM.grid(row=0,column=0, padx=100)
        self.textoCROP =tk.Label(self.FrameBotonSuperior, text="CROP - CULTIVO", font=("Arial",16,"bold"))
        self.textoCROP.grid(row=0,column=6, padx=100)
        
        self.botonNuevo= tk.Button(self.FrameBotonSuperior, text="NUEVO", bg="red", command=self.funcionNuevo)
        self.botonNuevo.grid(row=0,column=1, padx=5)
        self.botonNuevo.config(font=("Arial",10, "bold"))
        
        self.botonGuardar= tk.Button(self.FrameBotonSuperior, text="GUARDAR FARM", bg="red", command=self.guardarFuncion)
        self.botonGuardar.grid(row=0,column=2,padx=5)
        self.botonGuardar.config(font=("Arial",10, "bold"))
        self.botonGuardar2= tk.Button(self.FrameBotonSuperior, text="GUARDAR CROP", bg="red", command=self.guardar)
        self.botonGuardar2.grid(row=0,column=3,padx=5)
        self.botonGuardar2.config(font=("Arial",10, "bold"))
        self.botonGuardar3 =tk.Button(self.FrameBotonSuperior, text="GUARDAR TODO", bg= "red", command=self.combinar)
        self.botonGuardar3.grid(row=0, column=4, padx=5)
        self.botonGuardar3.config(font=("Arial",10, "bold"))
        
        self.botonCancelar= tk.Button(self.FrameBotonSuperior, text="CANCELAR", bg="red",command=self.funcionCancelar)
        self.botonCancelar.grid(row=0,column=5, padx=5)
        self.botonCancelar.config(font=("Arial",10, "bold"))
    
    #FUNCION PARA INABILITAR LOS CAMPOS DE TEXTO
    def funcionCancelar(self):
        self.varNombre.set('')
        self.varTamaño.set('')
        self.varNombreCultivo.set('')
        self.varCultivoId.set('')      
        self.cuadroNombreGranja.config(state='disabled')
        self.cuadroTamañoGranja.config(state='disabled')
        self.cuadroNombreCultivo.config(state='disabled')
        self.cuandroIdCultivo.config(state='disabled') 
        self.botonGuardar.config(state='disabled')
        self.botonCancelar.config(state='disabled')
        
    #FUNCION PARA ELIMINAR LOS CARACTERES EN LOS CAMPOS DE TEXTO
    def funcionNuevo(self):
        self.varNombre.set('')
        self.varTamaño.set('')
        self.varNombreCultivo.set('')  
        self.varCultivoId.set('')       
        self.cuadroNombreGranja.config(state='normal')
        self.cuadroTamañoGranja.config(state='normal')
        self.cuadroNombreCultivo.config(state='normal')  
        self.cuandroIdCultivo.config(state='normal')    
        self.botonGuardar.config(state='normal')
        self.botonCancelar.config(state='normal')
    
    #FUNCION PARA GUARDAR LOS CULTIVOS
    def guardar(self):
        if self.varNombreCultivo.get()!="" and self.varCultivoId.get()!=0:
            self.Crop.cultivoNombre = self.varNombreCultivo.get()
            self.Crop.cultivoId=self.varCultivoId.get()
            self.Crop.agregarDatos()
            self.vincular()
            messagebox.showinfo("EXITO", "SE GUARDO LOS DATOS DEL CULTIVO DE MANERA CORRECTA")
        else:
            messagebox.showerror("ERROR","NO SE PUDO INGRESAR LA INFORMACION, FALTAN CAMPOS POR LLENAR")
    #FUNCION PARA GUARDAR LAS GRANJAS  
    def guardarFuncion(self):
        if self.varNombre.get()!="" and self.varTamaño.get()!=0:    
            self.Farm.nombreGranja = self.varNombre.get()
            self.Farm.tamañoGranja=self.varTamaño.get()
            self.Farm.agregarDatos()
            self.vincularTabla()
            messagebox.showinfo("EXITO", "SE GUARDO LOS DATOS DE LA GRANJA DE MANERA CORRECTA")
        else:
            messagebox.showerror("ERROR","NO SE PUDO INGRESAR LA INFORMACION, FALTAN CAMPOS POR LLENAR")
    #FUNCION PARA GUARDAR TODO  
    def combinar (self):
        self.guardarFuncion()
        self.guardar()
    #VINCULA EN LA INTERFAZ LA TABLA DE BASE DE DATOS CULTIVOS  
    def vincular (self):
        self.Crop.verTabla()
        self.baseDeDatosCrop.delete(*self.baseDeDatosCrop.get_children())
        for fila2 in self.Crop.datosEnLaTabla:
            self.baseDeDatosCrop.insert("","end",values=fila2)
    #VINCULA EN LA INTERFAZ LA TABLA DE BASE DE DATOS GRANJA    
    def vincularTabla(self):
        self.Farm.verTabla()
        self.tablaBaseDeDatos.delete(*self.tablaBaseDeDatos.get_children())
        for fila in self.Farm.datosEnLaTabla:
            self.tablaBaseDeDatos.insert("","end",values=fila)
        
    #CREACION DE BOTONES INFERIORES
    def botonInferior(self):
        self.varId =tk.IntVar()
        self.textoId = tk.Label(self.FrameBotonInferior, text="ID: ")
        self.textoId.grid(row=0,column=0,padx=5,pady=5,columnspan=2)
        self.cuadroId = tk.Entry(self.FrameBotonInferior, textvariable=self.varId)
        self.cuadroId.grid(row=1,column=0,padx=5,pady=5, columnspan=2)
        
        self.botonEditar= tk.Button(self.FrameBotonInferior, text="EDITAR", bg="red",font=("Arial",10, "bold"), command=self.funcionEditar)
        self.botonEditar.grid(row=2,column=0, padx=0,pady=5)
        
        self.botoneliminar= tk.Button(self.FrameBotonInferior, text="ELIMINAR", bg="red",font=("Arial",10, "bold"),command=self.funcionEliminar)
        self.botoneliminar.grid(row=2,column=1,padx=5,pady=5)
        
    def funcionEditar(self):
        if self.varNombre.get():
            self.Farm.actualizarDatos("Granja","NOMBRE_GRANJA",self.varNombre.get(),self.varId.get())
        elif self.varTamaño.get():
            self.Farm.actualizarDatos("Granja","SIZE_GRANJA",self.varTamaño.get(),self.varId.get())    
        self.vincularTabla()
        if self.varNombreCultivo.get():    
            self.Crop.actualizarDatos("Cultivos","NOMBRE_CULTIVO",self.varNombreCultivo.get(),self.varId.get())
        elif self.varCultivoId.get():
            self.Crop.actualizarDatos("Cultivos","ID_CULTIVO", self.varCultivoId.get(),self.varId.get())
        self.vincular()
        
    def funcionEliminar(self):
        self.Farm.eliminarDatos("Granja",self.varId.get())
        self.Farm.eliminar_cultivos(self.varId.get())
        self.vincularTabla()
        self.vincular()
        
aplicacion = interfaz() 
