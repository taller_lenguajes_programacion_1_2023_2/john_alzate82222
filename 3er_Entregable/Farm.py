from conexion import Conexion

class farm(Conexion):
    def __init__(self, nombreGranja="", tamañoGranja=0):
        Conexion.__init__(self)
        self.nombreGranja = nombreGranja
        self.tamañoGranja = tamañoGranja
        self.crearTabla("Granja","Farm")
    
    def agregarDatos(self):
        datos = [
            (f"{self.nombreGranja}",f"{self.tamañoGranja}")
        ]
        if self.nombreGranja !="" and self.tamañoGranja!=0:
            self.insertarDato("Granja",2,datos)

        
    def verTabla(self):
        self.datosEnLaTabla = self.seleccionarTabla("Granja")
    
    def eliminar_cultivos(self, id_granja):
        query= "DELETE FROM Cultivos WHERE {}={}".format("ID_CULTIVO", id_granja)
        self.Apuntador.execute(query)
        self.BaseDeDatos.commit()