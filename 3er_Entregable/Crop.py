from conexion import Conexion

class crop (Conexion):
    def __init__(self, cultivoNombre="", cultivoId =0):
        Conexion.__init__(self)
        self.cultivoNombre = cultivoNombre
        self.cultivoId = cultivoId
        self.crearTabla("Cultivos","Crop")
        
    def agregarDatos(self):
        
        datos = [
            (f"{self.cultivoNombre}",f"{self.cultivoId}")
        ]
        if self.cultivoNombre !="" and self.cultivoId!=0:
            self.insertarDato("Cultivos",2,datos)
        
    def verTabla(self):
        self.datosEnLaTabla = self.seleccionarTabla("Cultivos") 
    