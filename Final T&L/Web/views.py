from django.shortcuts import render,redirect
from .models import usuario

# Create your views here.
def pagInicio(request):
    return render(request, 'index.html')

def login(request):
    mensajeError = ""
    if request.method == 'POST':
        usuarioo = request.POST['usuario']
        clave = request.POST['pasword']
        
        
        try:
            existente = usuario.objects.get(USUARIO = usuarioo)
            if existente.CONTRASENA == clave:
                return redirect('/Servicios/')
            else:
                mensajeError = "Contraseña Incorrecta."
                
        except:
            mensajeError = "Usuario No Encontrado."
        
    return render(request, 'login.html', {
        "error": mensajeError
    })


def registro(request):
    mensajeFail=""
    if request.method == 'POST':
        nombre= request.POST['nombre']
        apellido= request.POST['apellido']
        documento= request.POST['documento']
        celular= request.POST['celular']
        usuarioo= request.POST['usuario']
        clave= request.POST['pasword']
        
        try:
            existente = usuario.objects.get(USUARIO=usuarioo)
            mensajeFail="El Usuario Ya Existe"
        except:
            nuevoUsuario = usuario.objects.create(NOMBRE=nombre, APELLIDO=apellido,DOCUMENTO=documento,
                                               CELULAR=celular,USUARIO=usuarioo,CONTRASENA=clave)
            return redirect('/Login/')
        
    return render(request,'registro.html', {
        "error": mensajeFail
    })
