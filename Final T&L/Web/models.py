from django.db import models

# Create your models here.
class usuario (models.Model):
    ID = models.IntegerField(primary_key=True)
    NOMBRE=models.CharField(max_length=50)
    APELLIDO=models.CharField(max_length=50)
    DOCUMENTO=models.CharField(max_length=15)
    CELULAR=models.CharField(max_length=15)
    USUARIO=models.CharField(max_length=50)
    CONTRASENA=models.CharField(max_length=50)

