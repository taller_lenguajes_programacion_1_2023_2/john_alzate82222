from django.shortcuts import render

# Create your views here.

def servicios(request):
    return render(request,'servicios.html')

def compras(request,compra):
    return render(request,'compra.html',{
        "compra":compra
    })
def compras2(request,compra2):
    return render(request,'compra2.html',{
        "compra2":compra2
    })
