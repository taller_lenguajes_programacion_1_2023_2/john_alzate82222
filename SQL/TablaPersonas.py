from sqlmain import BaseDeDAtos

class TablaPersonas(BaseDeDAtos):
    def __init__ (self):
        super().__init__()
    
    def CrarTablaPersonas(self):
        self.CrearTabla( "personas", "ColumnaPersonas" )
    
    def AgregarUnDato(self, cedula, nombre, apellido):
        Datos=[
            (f"{cedula}",f"{nombre}",f"{apellido}")
        ]
        self.insertarDatos("personas","?,?,?",Datos)