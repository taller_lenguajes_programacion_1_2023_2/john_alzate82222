from sqlmain import BaseDeDAtos

class TablaEstudios(BaseDeDAtos):
    def __init__ (self):
        super().__init__()
    
    def CrarTablaEstudios(self):
        self.CrearTabla( "Estudios", "ColumnaEstudios" )
    
    def AgregarUnDato(self, carrera, fi, ff):
        Datos=[
            (f"{carrera}",f"{fi}",f"{ff}")
        ]
        self.insertarDatos("Estudios","?,?,?",Datos)