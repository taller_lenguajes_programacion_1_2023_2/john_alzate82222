import sqlite3
import json
#Query =consulta en base de datos= eliminar, cambiar, consultar
class BaseDeDAtos:
    def __init__(self):
        self.MiBaseDeDatos= sqlite3.connect("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/SQL/BaseDeDatos.sqlite")
        self.Apuntador= self.MiBaseDeDatos.cursor()
        with open("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/SQL/Query.json","r") as consultas:
            self.Queries = json.load(consultas)
            
    def CrearTabla (self, NombreTabla ="", Columnas=""):
        if NombreTabla != "" and Columnas != "":
            info=self.Queries["CrearTabla"].format(NombreTabla,self.Queries[Columnas])
            self.Apuntador.execute (info)
            self.MiBaseDeDatos.commit()
            print("mi base de datos: la tabla", NombreTabla, "se ha creado correctamente")
    
    def insertarDatos(self, tabla="", interrogantes="", valores=""):
        if tabla !="" and valores!="":
            info=self.Queries["InsertarDato"].format(tabla,interrogantes)
            self.Apuntador.executemany(info, valores)
            self.MiBaseDeDatos.commit()
    
#Apuntador.execute("CREATE TABLE Personas(Id INTEGER PRIMARY KEY AUTOINCREMENT ,Cedula INTEGER UNIQUE , Nombre VARCHAR(20) NOT NULL, Apellido TEXT)")
