from AdminSql import conexion

class estudiante(conexion):
    def __init__ (self, NombreE= "", Apellido= "", Identificacion= "", FechaIngreso ="",carrera ="", CorreoElectronicoE= "" ):
        self.Id= 1
        self.Nombre = NombreE
        self.Apellido = Apellido
        self.Identificacion= Identificacion
        self.FechaIngreso = FechaIngreso
        self.Carrera = carrera
        self.CorreoElectronico = CorreoElectronicoE
    def getNombre (self):
        return self.Nombre
    def setNombre (self, NuevoNombre):
        self.Nombre = NuevoNombre
    
    def getApellido (self):
        return self.Apellido
    def setDireccion (self, NuevoApellido):
        self.Apellido= NuevoApellido
    
    def getIdentificacion (self):
        return self.Identificacion
    def setRector (self, NuevaIdentificacion):
        self.Identificacion = NuevaIdentificacion
    
    def getFechaIngreso (self):
        return self.FechaIngreso
    def setFundacion (self, NuevaFechaI):
        self.FechaIngreso = NuevaFechaI
    
    def getCarrera (self):
        return self.Carrera
    def setTelefono (self, NuevaCarrera):
        self.Carrera = NuevaCarrera
    
    def getEmail (self):
        return self.CorreoElectronico
    def setEmail (self, NuevoEmail):
        self.CorreoElectronico= NuevoEmail
    
    def CrearTablaEstudiante(self):
        self.CrearTabla("Estudiante","ColumnaEstudiante")
    
    def AgregarDatos (self):
        datos=[
            (f"{self.Nombre}",f"{self.Apellido}",f"{self.Identificacion}",f"{self.FechaIngreso}",
             f"{self.Carrera}",f"{self.CorreoElectronico}")
        ]
        self.insertarDatos("Estudiante","?,?,?,?,?,?",datos)
    
    def EliminarDatos (self, Id):
        if self.Eliminar ("Estudiante",Id ):
            print ("ELIMINADO CORRECTAMENTE")
        else:
            print ("NO FUE ENCONTRADO")
    
        
        