import sqlite3
import json

class conexion:
    def __init__(self):
        self.MiBaseDeDatos= sqlite3.connect("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/2doEntregble/BaseDeDatos.sqlite")
        self.Apuntador= self.MiBaseDeDatos.cursor()
        with open("C:/Users/JOHN KIDER/OneDrive/Escritorio/taller de lenguaje/john_alzate82222/2doEntregble/Query.json","r") as consultas:
            self.Queries = json.load(consultas)
            
            
    def CrearTabla (self, NombreTabla ="", Columnas=""):
        if NombreTabla != "" and Columnas != "":
            info=self.Queries["Crear"].format(NombreTabla,self.Queries[Columnas])
            self.Apuntador.execute (info)
            self.MiBaseDeDatos.commit()
            print("mi base de datos: la tabla", NombreTabla, "se ha creado correctamente")
    
    def insertarDatos(self, tabla="", interrogantes="", valores=""):
        if tabla !="" and valores!="":
            info=self.Queries["Insertar"].format(tabla,interrogantes)
            self.Apuntador.executemany(info, valores)
            self.MiBaseDeDatos.commit()
            print("se agrego correctamente")
    
    def BuscarPorId (self,ID, NombreTabla ):
        Buscador=self.Queries["Ver"].format(NombreTabla,"Id",ID)
        self.Pointer.execute(Buscador)
        Buscador = self.Pointer.fetchone()[0]   
        return Buscador
        
    def Eliminar (self, NombreTabla="", Id=0):
        if NombreTabla!="" and Id!=0:
            if(self.BuscarPorId(Id, NombreTabla)):
                dato=self.Queries["Eliminar"].format(NombreTabla,"ID",Id)
                self.Pointer.execute(dato)                    
                self.MiBaseDeDatos.commit()
                return True
            else:
                print("NO ENCONTRADO")  
                return False
        else:
            print("ERROR")  
            return False   
        print("se elimino correctamente")