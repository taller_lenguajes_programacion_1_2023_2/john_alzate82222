from Universidad import universidad
from Estudiante import estudiante
from AdminSql import conexion
from Matricula import matricula


class Materia(matricula):
    def __init__(self,Nombre= "", Direccion= "", Rector= "", Fundacion ="",Telefono ="", CorreoElectronico= "",
                 NombreE= "", Apellido= "", Identificacion= "", FechaIngreso ="",carrera ="", CorreoElectronicoE= "",
                 FechaMatricula = "", Estudiante= estudiante, Universidad = universidad , NotaFinal= 0.0,
                 NombreM ="", Codigo= "", Semestre = 0, HorasSemanales =0, Programa= "", Creditos=0):
        super.__init__(Nombre, Direccion,Rector,Fundacion,Telefono,CorreoElectronico)
        super.__init__(NombreE,Apellido, Identificacion,FechaIngreso,carrera,CorreoElectronicoE)
        super.__init__(FechaMatricula, Estudiante, Universidad , NotaFinal)
        self.NombreM= NombreM
        self.Codigo=Codigo
        self.Semestre= Semestre
        self.HorasSemanales=HorasSemanales
        self.Programa=Programa
        self.Creditos= Creditos
        
    def getNombre (self):
        return self.NombreM
    def setNombre (self, NuevoNombre):
        self.NombreM = NuevoNombre
    
    def getCodigo (self):
        return self.Codigo
    def setDireccion (self, NuevoCodigo):
        self.Codigo= NuevoCodigo
    
    def getSemestre (self):
        return self.Semestre
    def setRector (self, NuevoSemestre):
        self.Semestre = NuevoSemestre
    
    def getHoras (self):
        return self.HorasSemanales
    def setFundacion (self, NuevoHoras):
        self.HorasSemanales = NuevoHoras
    
    def getTelefono (self):
        return self.Programa
    def setTelefono (self, NuevoPrograma):
        self.Programa = NuevoPrograma
    
    def getEmail (self):
        return self.Creditos
    def setEmail (self, NuevoCreditos):
        self.Creditos= NuevoCreditos
    
    def CrearTablaMateria(self):
        self.CrearTabla("Materia","ColumnaMateria")
    
    def AgregarDatos (self):
        datos=[
            (f"{self.NombreM}",f"{self.Codigo}",f"{self.Semestre}",f"{self.HorasSemanales}",
             f"{self.Programa}",f"{self.Creditos}")
        ]
        self.insertarDatos("Materia","?,?,?,?,?,?",datos)
    
    def EliminarDatos (self, Id):
        if self.Eliminar ("Materia",Id ):
            print ("ELIMINADO CORRECTAMENTE")
        else:
            print ("NO FUE ENCONTRADO")
        