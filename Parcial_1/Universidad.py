from AdminSql import conexion
class universidad (conexion):
    def __init__ (self, Nombre= "", Direccion= "", Rector= "", Fundacion ="",Telefono ="", CorreoElectronico= "" ):
        self.Id = 1
        self.Nombre = Nombre
        self.Direccion = Direccion
        self.Rector= Rector
        self.Fundacion = Fundacion
        self.Telefono = Telefono
        self.CorreoElectronico = CorreoElectronico
    def getNombre (self):
        return self.Nombre
    def setNombre (self, NuevoNombre):
        self.Nombre = NuevoNombre
    
    def getDireccion (self):
        return self.Direccion
    def setDireccion (self, nuevaDireccion):
        self.Direccion= nuevaDireccion
    
    def getRector (self):
        return self.Rector
    def setRector (self, NuevoRector):
        self.Rector = NuevoRector
    
    def getFundacion (self):
        return self.Fundacion
    def setFundacion (self, NuevaFundacion):
        self.Fundacion = NuevaFundacion
    
    def getTelefono (self):
        return self.Telefono
    def setTelefono (self, NuevoTelefono):
        self.Telefono = NuevoTelefono
    
    def getEmail (self):
        return self.CorreoElectronico
    def setEmail (self, NuevoEmail):
        self.CorreoElectronico= NuevoEmail
    
    def CrearTablaUniversidad(self):
        self.CrearTabla("Universidad","ColumnaUniversidad")
    
    def AgregarDatos (self):
        datos=[
            (f"{self.Nombre}",f"{self.Direccion}",f"{self.Rector}",f"{self.Fundacion}",
             f"{self.Telefono}",f"{self.CorreoElectronico}")
        ]
        self.insertarDatos("Universidad","?,?,?,?,?,?",datos)
    
    def EliminarDatos (self, Id):
        if self.Eliminar ("Universidad",Id ):
            print ("ELIMINADO CORRECTAMENTE")
        else:
            print ("NO FUE ENCONTRADO")
    

        