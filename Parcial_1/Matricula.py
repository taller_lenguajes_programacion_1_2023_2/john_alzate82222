from Universidad import universidad
from Estudiante import estudiante
from AdminSql import conexion

class matricula(universidad,estudiante, conexion):
   
    def __init__(self,Nombre= "", Direccion= "", Rector= "", Fundacion ="",Telefono ="", CorreoElectronico= "",
                 NombreE= "", Apellido= "", Identificacion= "", FechaIngreso ="",carrera ="", CorreoElectronicoE= "",
                 FechaMatricula = "", Estudiante= estudiante, Universidad = universidad , NotaFinal= 0.0):
        super.__init__(Nombre, Direccion,Rector,Fundacion,Telefono,CorreoElectronico)
        super.__init__(NombreE,Apellido, Identificacion,FechaIngreso,carrera,CorreoElectronicoE)
        self.FechaMatricula = FechaMatricula
        self.Estudiante = Estudiante
        self.Universidad = Universidad
        self.NotaFinal= NotaFinal
        
    def CrearTablaMatricula(self):
        self.CrearTabla("Matricula","ColumnaMatricula")
    
    def AgregarDatos (self):
        datos=[
            (f"{self.FechaMatricula}",f"{self.Estudiante}",f"{self.Universidad}",f"{self.NotaFinal}")
             
        ]
        self.insertarDatos("Matricula","?,?,?,?",datos)
    
    def EliminarDatos (self, Id):
        if self.Eliminar ("Matricula",Id ):
            print ("ELIMINADO CORRECTAMENTE")
        else:
            print ("NO FUE ENCONTRADO")
    
        