from InstrumentosMusicales import Instrumentos
import pandas as pd

class Piano (Instrumentos):
    #constructor
    def __init__(self,TIPO_INSTRUMENTO="",	NOMBRE="",	FAMILIA="",ESTADO="",MATERIAL="",SONIDO="",
                 color="", marca="", peso="", numero_octavas="", tipo_pedal="", numero_martillos=""):
        self.color= color
        self.marca = marca
        self.peso = peso
        self.numero_octavas= numero_octavas
        self.tipo_pedal= tipo_pedal
        self.numero_martillos = numero_martillos
        
        super(). __init__(TIPO_INSTRUMENTO, NOMBRE, FAMILIA, ESTADO, MATERIAL,SONIDO)
    #definicion getter and setter
    def getcolor(self):
        return self.color
    def setcolor (self, nuevocolor):
        self.color = nuevocolor
    
    def getmarca (self):
        return self.marca
    def setmarca (self, nuevamarca):
        self.marca= nuevamarca
    
    def getpeso(self):
        return self.peso
    def setpeso(self, nuevopeso):
        self.peso= nuevopeso
        
    def getnumero_octavas (self):
        return self.numero_octavas
    def setnumero_octavas (self, nuevooctavas):
        self.numero_octavas= nuevooctavas
    
    def gettipo_pedal(self):
        return self.tipo_pedal
    def settipo_pedal(self, nuevopedal):
        self.tipo_pedal= nuevopedal
        
    def getnumero_martillos(self):
        return self.numero_martillos
    def setnumero_martillos(self, nuevomartillos):
        self.numero_martillos= nuevomartillos
    
    #funciones de la clase hija
    def getExcelPianoHijo(self):
    
        self.PianoHijo=self.ReadHoja("ClaseHijo")
        if not self.PianoHijo.empty:
            print("\n")
            print(self.PianoHijo.to_string(index=False))      
        else:
            print("NO HAY DATOS") 

    def crearTablaPiano (self):
        self.CrearTabla("PianoHijo", "ColumnaPiano")
    
    def añadirDatosHijo (self, color, marca, peso, numero_octavas, tipo_pedal, numero_martillos):
        datos=[
            (f"{self.color}",f"{self.marca}",f"{self.peso}",f"{self.numero_octavas}",
             f"{self.tipo_pedal}",f"{self.numero_martillos}")
        ]    
        self.insertarDatos("PianoHijo","?,?,?,?,?,?",datos)
    
    
    def agregarExcelHijo(self):
        if self.agregar_Excel_padre():
            self.PianoHijo=self.ReadHoja("ClaseHijo")
            PianoDatos=pd.DataFrame({
                "ID":[self.Id],
                "TIPO INSTRUMENTO":[self.getTIPO_INSTRUMENTO()],
                "NOMBRE":[self.getNOMBRE()],
                "FAMILIA":[self.getFAMILIA()],
                "ESTADO":[self.getESTADO()],
                "MATERIAL":[self.getMATERIAL()],
                "SONIDO":[self.getSONIDO()],
                "COLOR":[self.getcolor()],
                "MARCA":[self.getmarca()],
                "PESO":[self.getpeso()],
                "NUMERO OCTAVAS":[self.getnumero_octavas()],
                "TIPO DE PEDAL":[self.gettipo_pedal()],
                "NUMERO MARTILLOS":[self.getnumero_martillos()]
            })
            self.piano=pd.concat([self.piano,PianoDatos])        
            with pd.ExcelWriter(self.xlsl) as writer:
                self.Padre.to_excel(writer, sheet_name="ClasePadre", index=False)
                self.piano.to_excel(writer, sheet_name="ClaseHijo", index=False)
                
            print("SE AGREGO CORRECTAMENTE")
    
    def EliminarExcelHijo(self, Id):
       
        if self.eliminar_Excel_Padre(Id):
            self.PianoHijo=self.ReadHoja("ClaseHijo")
            self.PianoHijo = self.PianoHijo[self.PianoHijo["ID"] != Id]
            with pd.ExcelWriter(self.xlsl) as writer:
                self.Padre.to_excel(writer, sheet_name="ClasePadre", index=False)
                self.PianoHijo.to_excel(writer, sheet_name="ClaseHijo", index=False)  
            self.Eliminar("PianoHijo",Id) 
            print("ELIMINADO CORRECTAMENTE")   
            
    def Modificar(self, Nuevo, Id=0, Columna=""):
        
        self.PianoHijo=self.ReadHoja("ClaseHijo")
        columnas = ["COLOR", "MARCA", "PESO", "NUMERO OCTAVAS", "TIPO DE PEDAL", "NUMERO MARTILLOS"]
        if self.ModificarPadre(Nuevo,Id,Columna): 
            if Columna in columnas:
                self.Actualizar(Nuevo,"PianoHijo",Columna,Id) 
            Estatica=self.PianoHijo.loc[self.PianoHijo["ID"] == Id]
            Estatica=Estatica[Columna]
            self.PianoHijo[Columna] = self.PianoHijo[Columna].replace({Estatica.iloc[0]: Nuevo})             
            with pd.ExcelWriter(self.xlsl) as writer:
                self.Padre.to_excel(writer, sheet_name="ClasePadre", index=False)
                self.PianoHijo.to_excel(writer, sheet_name="ClaseHijo", index=False)    
            print("ACTUALIZADO CORRECTAMENTE")   


    
         