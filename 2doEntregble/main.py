from Piano import Piano

musica = Piano()
musica.CrearTablaInstrumentos()
musica.crearTablaPiano()

def imprimir_menu():
    print("Menú de opciones")
    print("1. Agregar datos")
    print("2. Mostrar datos")
    print("3. Eliminar datos")
    print("4. Actualizar o modificar datos")
    print("5. Salir")

# Definir una función para agregar datos
def agregarDatos():
    # Solicitar al usuario los datos a agregar
    tipoInstrumento= input("INGRESE EL TIPO DE INSTRUMENTO: ")
    nombre= input("INGRESE EL NOMBRE DEL INSTRUMENTO: ")
    familia = input("INGRESE LA FAMILIA DEL INSTRUMENTO: ")
    estado = input("INGRESE EL ESTADO DEL INSTRUMENTO")
    material = input("INGRESE EL MATERIAL DE FABRICACION")
    sonido = input("INGRESE EL TIPO DE SONIDO DEL INSTRUMENTO 'AGUDO, GRAVE'")
    color= input("DIGITE EL COLOR DEL INSTRUMENTO")
    marca = input ("DIGITE LA MARCA DEL INSTRUMENTO")
    peso = input("DIGITE EL PESO DEL INSTRUMENTO")
    numeroOctavas = input("DIGITE EL NUMERO DE OCTAVAS DEL INSTRUMENTO")
    tipoPedal = input("INGRESE EL TIPO DE PEDAL")
    numeroMartillos = input ("DIGITE LA CANTIDAD DE MARTILLOS QUE TIENE EL INSNTRUMENTO")
    
    musica.AgregarDatosTabla(tipoInstrumento,nombre,familia,estado,material,sonido)
    musica.añadirDatosHijo(color,marca,peso,numeroOctavas,tipoPedal,numeroMartillos)
    musica.agregarExcelHijo()
    
    
# Definir una función para mostrar datos
def mostrar_datos():
    print ("1. CLASE PADRE \n2. CLASE HIJO")
    print("\n")
    opcion = int(input("INGRESE EL NUMERO DE LA OPCION QUE QUIERE REALIZAR"))
    
    if opcion == 1:
        musica.VerExcel()
    elif opcion == 2:
        musica.getExcelPianoHijo()
    else:
        print ("OPCION NO VALIDA")
    
# Definir una función para eliminar datos
def eliminar_datos():
    iD = int(input("DIGITE LA ID QUE DESEA ELIMINAR"))
    musica.EliminarExcelHijo(iD)

# Definir una función para actualizar o modificar datos
def actualizar_datos():
    iD = int(input ("INGRESE EL ID DE LOS DATOS QUE DESEA MODIFICAR"))
    musica.BuscarPorId(iD)
    
    print ("1. TIPO DE INSTRUMENTO")
    print ("2. NOMBRE")
    print ("3. FAMILIA")
    print ("4. ESTADO")
    print ("5. MATERIAL")
    print ("6. SONIDO")
    print ("7. COLOR")
    print ("8. MARCA")
    print ("9. PESO")
    print ("10. NUMERO OCTAVAS")
    print ("11. TIPO DE PEDAL")
    print ("12. NUMERO DE MARTILLOS")
    
    option= int(input("INGRESE EL NUMERO DE LA OPCION QUE VA A MODIFICAR"))
    if option == 1:
        nuevoTipo= input("INGRESE EL TIPO DE INSTRUMENTO NUEVO")
        musica.Modificar(nuevoTipo, iD, "TIPO INSTRUMENTO")
    elif option == 2:
        nuevoNombre= input("INGRESE EL NOMBRE DEL NUEVO INSTRUMENTO")
        musica.Modificar(nuevoNombre, iD, "NOMBRE")
    elif option == 3:
        nuevoFamilia= input("INGRESE LA FAMILIA NUEVA DEL INSTRUMENTO")
        musica.Modificar(nuevoFamilia, iD, "FAMILIA")
    elif option == 4:
        nuevoEstado= input("INGRESE EL ESTADO DEL INSTRUMENTO")
        musica.Modificar(nuevoEstado, iD, "ESTADO")
    elif option == 5:
        nuevoMaterial= input("INGRESE EL MATERIAL DEL INSTRUMENTO")
        musica.Modificar(nuevoMaterial, iD, "MATERIAL")
    elif option == 6:
        nuevoSonido= input("INGRESE SONIDO NUEVO DEL INSTRUMENTO")
        musica.Modificar(nuevoSonido, iD, "SONIDO")
    elif option == 7:
        nuevoColor= input("INGRESE EL COLOR DEL INSTRUMENTO")
        musica.Modificar(nuevoColor, iD, "COLOR")
    elif option == 8:
        nuevoMarca= input("INGRESE LA NUEVA MARCA DEL INSTRUMENTO")
        musica.Modificar(nuevoMarca, iD, "MARCA")
    elif option == 9:
        nuevoPeso= input("INGRESE EL NUEVO PESO INSTRUMENTO")
        musica.Modificar(nuevoPeso, iD, "PESO")
    elif option == 10:
        nuevoNOctavas= input("INGRESE EL NUMERO DE OCTAVAS DEL INSTRUMENTO")
        musica.Modificar(nuevoNOctavas, iD, "NUMERO OCTAVAS")
    elif option == 11:
        nuevoTPedal= input("INGRESE EL TIPO DE PEDAL DEL INSTRUMENTO")
        musica.Modificar(nuevoTPedal, iD, "TIPO DE PEDAL")
    elif option == 12:
        nuevoNMartillo= input("INGRESE EL NUMERO DE MARTILLOS DEL INSTRUMENTO")
        musica.Modificar(nuevoNMartillo, iD, "NUMERO MARTILLOS")
    else:
        print("OPCION NO VALIDA, INTENTE NUEVAMENTE")
    

# Bucle principal
while True:
    # Imprimir el menú
    imprimir_menu()

    # Solicitar al usuario una opción
    opcion = input("Ingrese una opción: ")

    # Validar la opción
    if opcion == "1":
        # Agregar datos
        agregarDatos()
        
    elif opcion == "2":
        # Mostrar datos
        mostrar_datos()
    elif opcion == "3":
        # Eliminar datos
        eliminar_datos()
    elif opcion == "4":
        # Actualizar o modificar datos
        actualizar_datos()
    elif opcion == "5":
        # Salir
        break
    else:
        # Opción no válida
        print("Opción no válida.")