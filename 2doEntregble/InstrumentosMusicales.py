from Tabla import excel
from SqlMain import conexion
import pandas as pd

#definicion de clasePadre
class Instrumentos(excel, conexion):
    def __init__(self,TIPO_INSTRUMENTO ="",	NOMBRE ="",	FAMILIA="",ESTADO="",MATERIAL="",SONIDO=""):
        conexion. __init__(self)
        super(). __init__()
        self.Id =1
        self.TIPO_INSTRUMENTO = TIPO_INSTRUMENTO
        self.NOMBRE = NOMBRE
        self.FAMILIA = FAMILIA
        self.ESTADO = ESTADO
        self.MATERIAL = MATERIAL
        self.SONIDO= SONIDO
        
    #definir getter y setter
    def getTIPO_INSTRUMENTO (self):
        return self.TIPO_INSTRUMENTO
    def setTIPO_INSTRUMENTO(self, nuevoInstrumento):
        self.TIPO_INSTRUMENTO= nuevoInstrumento
    
    def getNOMBRE(self):
        return self.NOMBRE
    def setNOMBRE(self, nuevoNombre):
        self.NOMBRE= nuevoNombre
    
    def getFAMILIA (self):
        return self.FAMILIA
    def setFAMILIA (self, nuevoFamilia):
        self.FAMILIA = nuevoFamilia
    
    def getESTADO (self):
        return self.ESTADO
    def setESTADO (self, nuevoEstado):
        self.ESTADO= nuevoEstado
    
    def getMATERIAL (self):
        return self.MATERIAL
    def setMATERIAL (self, nuevoMaterial):
        self.MATERIAL = nuevoMaterial
    
    def getSONIDO (self):
        return self.SONIDO
    def setSONIDO (self, nuevoSonido):
        self.SONIDO = nuevoSonido
    #funciones para crear, agregar, ver, eliminar, modificar en la base de datos y el excel
    def VerExcel(self):
        
        self.Padre=self.ReadHoja("ClasePadre")
        if not self.Padre.empty:
            print(self.Padre.to_string(index=False))
        else:
            print("SIN DATOS")  
               

    
    def CrearTablaInstrumentos(self):
        self.CrearTabla("InstrumentosPadre","ColumnasInstrumentos")

    def AgregarDatosTabla (self,TIPO_INSTRUMENTO, NOMBRE, FAMILIA, ESTADO, MATERIAL, SONIDO):
        datos=[
            (f"{self.TIPO_INSTRUMENTO}",f"{self.NOMBRE}",f"{self.FAMILIA}",f"{self.ESTADO}",
             f"{self.MATERIAL}",f"{self.SONIDO}")
        ]
        self.insertarDatos("InstrumentosPadre","?,?,?,?,?,?",datos)

    def agregar_Excel_padre(self):
     
        self.Padre=self.ReadHoja("ClasePadre")
        if not self.Padre.empty:
            self.Apuntador.execute("SELECT * FROM sqlite_sequence WHERE seq")
            self.Id = self.Apuntador.fetchmany()[0][1]      
            self.Id=int(self.Id)+1
        if(self.TIPO_INSTRUMENTO not in self.Padre["TIPO INSTRUMENTO"].values and self.TIPO_INSTRUMENTO!=""):
            datosClasePadre=pd.DataFrame({
                "ID":[self.Id],
                "TIPO INSTRUMENTO":[self.getTIPO_INSTRUMENTO()],
                "NOMBRE":[self.getNOMBRE()],
                "FAMILIA":[self.getFAMILIA()],
                "ESTADO":[self.getESTADO()],
                "MATERIAL":[self.getMATERIAL()],
                "SONIDO":[self.getSONIDO()]
            })
            self.Padre=pd.concat([self.Padre,datosClasePadre])   
            
            return True
        else:
            print("YA EXISTE EL DATO O NO FUE INGRESADO, VERIFIQUE") 
            return False
    
    def eliminar_Excel_Padre(self, ID):
   
        if self.Eliminar("InstrumentosPadre",ID): 
            self.Padre=self.ReadHoja("ClasePadre")        
            self.Padre = self.Padre[self.Padre["ID"] != ID]
            return True
        else:
            return False
    def ModificarPadre(self, newDate, Id=0, Valores=""):
  
        self.Padre=self.ReadHoja("ClasePadre")
        Encabezados = ["TIPO INSTRUMENTO", "NOMBRE", "FAMILIA", "ESTADO", "MATERIAL", "SONIDO"]
        if Valores in Encabezados:
            if Valores=="TIPO INSTRUMENTO":
                if newDate not in self.Padre["TIPO INSTRUMENTO"].values and newDate!="":
                    if self.Actualizar(newDate,"InstrumentosPadre",Valores,Id): 
                        Estatico=self.Padre.loc[self.Padre["ID"] == Id]
                        Estatico=Estatico[Valores]
                        self.Padre[Valores] = self.Padre[Valores].replace({Estatico.iloc[0]: newDate})   
                        return True 
                else:
                    print("YA FUE INGRESADO O NO LLENO EL CAMPO") 
                    return False
            else:
                if self.Actualizar(newDate,"InstrumentosPadre",Valores,Id): 
                    Estatico=self.Padre.loc[self.Padre["ID"] == Id]
                    Estatico=Estatico[Valores]
                    self.Padre[Valores] = self.Padre[Valores].replace({Estatico.iloc[0]: newDate})   
                    return True                     
        else:
            return True                
        



        